//
//  MyClasses.swift
//  empresas-ioasys
//
//  Created by RichardDev on 10/28/18.
//  Copyright © 2018 RichardDev. All rights reserved.
//

import Foundation
import UIKit

struct Empresa : Mappable {
    
    var id: Int
    var name: String
    var description: String
    var data: String
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("name")
        self.description = mapper.keyPath("description")
        self.data = mapper.keyPath("data")
    }
}

class MyAuthenticationAPI : APIRequest {
    
    @discardableResult
    static func loginWith(username: String, password: String, callback: ResponseBlock<User>?) -> AuthenticationAPI {
        
        let request = AuthenticationAPI(method: .post, path: "login", parameters: ["username": username, "password": password], urlParameters: nil, cacheOption: .networkOnly){ (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let user = User(dictionary: response)
                callback?(user, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
    
}
