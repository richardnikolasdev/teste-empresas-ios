//
//  MainHeader.swift
//  empresas-ioasys
//
//  Created by RichardDev on 10/28/18.
//  Copyright © 2018 RichardDev. All rights reserved.
//

import UIKit

class MainHeader : UIView {
    
    init(frame: CGRect = .zero, title: String) {
        super.init(frame: frame)
        if frame == .zero {
            translatesAutoresizingMaskIntoConstraints = false
        }
        
        backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) deu ruim demais")
    }
}
