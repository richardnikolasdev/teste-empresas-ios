//
//  ViewController.swift
//  empresas-ioasys
//
//  Created by RichardDev on 10/27/18.
//  Copyright © 2018 RichardDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var emailField : UITextField!
    @IBOutlet var passwordField : UITextField!
    
    @discardableResult
    @IBAction func login () {
        let defaultValue = "Oh boy"
        let email = emailField.text
        let password = passwordField.text
        
        let responseUser = """
            {
            "id": 1,
            "email": \(email ?? defaultValue) ,
            "password": \(password ?? defaultValue)
            }
            """
        
        var usuario = User(mapper: Mapper(dictionary: [responseUser: responseUser]))
        
        // var myResponseBlock = APIRequest.ResponseBlock(usuario, API.RequestError, true)
        
        /* MyAuthenticationAPI.loginWith(username: email ?? defaultValue, password: password ?? defaultValue, callback: APIRequest.ResponseBlock<User>) */
        
        print("The email is: \(email ?? defaultValue) and the password is \(password ?? defaultValue)")
    }
    
}



