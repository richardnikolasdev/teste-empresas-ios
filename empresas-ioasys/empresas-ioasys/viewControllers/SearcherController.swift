//
//  SearcherController.swift
//  empresas-ioasys
//
//  Created by RichardDev on 10/28/18.
//  Copyright © 2018 RichardDev. All rights reserved.
//

import UIKit

class SearcherController : UIViewController {
    
    // let image = UIImage()
    let header = MainHeader(title: "ioasys")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(header)
        header.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        header.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        header.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        header.heightAnchor.constraint(equalToConstant: 70).isActive = true
    }
}
